FROM docker.io/library/centos:6.10

LABEL description="Post-EOL CentOS 6 Base Image"
LABEL maintainer="nicolas.bonneval@enreach.com"
LABEL version="6.10.1"

# Remove obsolete repos ...
RUN rm -fv /etc/yum.repos.d/CentOS-*.repo
# ... and use archived repos instead (mirrors listed @ https://vault.centos.org/)
COPY centos-6-vault.repo /etc/yum.repos.d/
# Update original base image with latest updates
RUN yum -y upgrade && yum clean all

